FROM python:3.10-bullseye

WORKDIR /root
ARG DEBIAN_FRONTEND=noninteractive

# install system dependencies
# note: graphviz is needed for for building graphs in the sphinx documentations
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y apt-utils && \
    apt-get install -y git python3-dev python3-venv python3-tk gcc g++ libeigen3-dev libicu-dev libgmp-dev libmpfr-dev libcgal-dev gmsh libfreetype6-dev libxml2-dev libxslt-dev && \
    apt-get install graphviz -y && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# dowload pyproject and install python requirements
# note: for some reason src folder is needed
RUN wget https://gitlab.com/spam-project/spam/-/raw/master/pyproject.toml && \
    pip install pip-tools && \
    mkdir src && \
    pip-compile -o requirements.txt pyproject.toml --all-extras && \
    pip install -r requirements.txt && \
    rm -rv src
