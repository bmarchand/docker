#!/bin/bash

# Fonction pour modifier l'UID et le GID
change_uid_gid() {
    USER=glorfindel
    NEW_UID=${UID:-1000}
    NEW_GID=${GID:-1000}

    if [ "$NEW_UID" != "1000" ] || [ "$NEW_GID" != "1000" ]; then
        echo "Changing UID and GID to $NEW_UID and $NEW_GID..."
        groupmod -g $NEW_GID $USER
        usermod -u $NEW_UID -g $NEW_GID $USER
    fi
}

# Appeler la fonction pour modifier l'UID et le GID
change_uid_gid

# Exécuter la commande
exec gosu glorfindel "$@"